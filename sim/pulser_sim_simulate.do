######################################################################
#
# File name : pulser_sim_simulate.do
# Created on: Wed Jun 03 12:51:28 EDT 2020
#
# Auto generated by Vivado for 'behavioral' simulation
#
######################################################################
vsim -voptargs="+acc" -L xil_defaultlib -L secureip -lib xil_defaultlib xil_defaultlib.pulser_sim

set NumericStdNoWarnings 1
set StdArithNoWarnings 1

do {pulser_sim_wave.do}

view wave
view structure
view signals



run -all
run -all
run -all
