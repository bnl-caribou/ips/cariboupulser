----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 28.09.2017 16:06:19
-- Design Name: 
-- Module Name: pulser_sim - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library uvvm_util;
context uvvm_util.uvvm_util_context;

library uvvm_vvc_framework;
use uvvm_vvc_framework.ti_vvc_framework_support_pkg.all;

library bitvis_vip_axilite;
context bitvis_vip_axilite.vvc_context;
use bitvis_vip_axilite.axilite_bfm_pkg.init_axilite_if_signals;
 
library bitvis_vip_clock_generator;
context bitvis_vip_clock_generator.vvc_context;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library xil_defaultlib;
use xil_defaultlib.all;

use std.env.stop;

entity pulser_sim is
--  Port ( );
end pulser_sim;

architecture Behavioral of pulser_sim is
	
signal ccpd_inj_pulse_1_p     :std_logic;
signal ccpd_inj_pulse_2_p       :std_logic;
signal ccpd_inj_pulse_3_p      :std_logic;
signal ccpd_inj_pulse_4_p       :std_logic;
signal ccpd_inj_pulse_1_n     :std_logic;
signal ccpd_inj_pulse_2_n       :std_logic;
signal ccpd_inj_pulse_3_n      :std_logic;
signal ccpd_inj_pulse_4_n       :std_logic;
signal ccpd_inj_ref           :std_logic;

signal global_reset :std_logic := '0';
signal clk      :std_logic; -- H35DEMO config. LD_CMOS
signal trigger_inj : std_logic := '0';

constant PERIOD : time := 6.25 ns;
constant C_ADDR_WIDTH : integer := 6;
constant C_DATA_WIDTH : integer := 32;

signal pls_intf : t_axilite_if ( write_address_channel( awaddr( C_ADDR_WIDTH-1 downto 0)),
                                 write_data_channel( wdata ( C_DATA_WIDTH-1 downto 0), wstrb(( C_DATA_WIDTH/8)-1 downto 0)),
                                 read_address_channel ( araddr( C_ADDR_WIDTH-1 downto 0)),
                                 read_data_channel( rdata ( C_DATA_WIDTH-1 downto 0)) ) := init_axilite_if_signals(C_ADDR_WIDTH, C_DATA_WIDTH);

signal axilite_config : t_axilite_bfm_config;

begin


trigger_inj <= '0';
axilite_config.max_wait_cycles             <= 1;
axilite_config.max_wait_cycles_severity    <= TB_FAILURE;
axilite_config.clock_period                <= PERIOD;
axilite_config.clock_period_margin         <= 0 ns;
axilite_config.clock_margin_severity       <= TB_ERROR;
axilite_config.setup_time                  <= -1 ns;
axilite_config.hold_time                   <= -1 ns;
axilite_config.bfm_sync                    <= SYNC_ON_CLOCK_ONLY;
--axilite_config.expected_response           <= OKAY;
axilite_config.expected_response_severity  <= TB_FAILURE;
--axilite_config.protection_setting          <= UNPRIVILIGED_UNSECURE_DATA;
axilite_config.num_aw_pipe_stages          <= 1;
axilite_config.num_w_pipe_stages           <= 1;
axilite_config.num_ar_pipe_stages          <= 1;
axilite_config.num_r_pipe_stages           <= 1;
axilite_config.num_b_pipe_stages           <= 1;
axilite_config.id_for_bfm                  <= ID_BFM;
axilite_config.id_for_bfm_wait             <= ID_BFM_WAIT;
axilite_config.id_for_bfm_poll             <= ID_BFM_POLL;



-- uvvm engine
i_ti_uvvm_engine : entity uvvm_vvc_framework.ti_uvvm_engine;

--clock generation
i_clk_gen_vvc : entity bitvis_vip_clock_generator.clock_generator_vvc
generic map(
    GC_INSTANCE_IDX => 1,
    GC_CLOCK_NAME   => "clock",
    GC_CLOCK_PERIOD => PERIOD,
    GC_CLOCK_HIGH_TIME => PERIOD/2 
)
port map( 
    CLK => clk
);

-- AXI-lite VVC
i_vvc_axilite_master : entity bitvis_vip_axilite.axilite_vvc
generic map (
    GC_ADDR_WIDTH                           => C_ADDR_WIDTH,
    GC_DATA_WIDTH                           => C_DATA_WIDTH,
    GC_INSTANCE_IDX                         =>  2,
    GC_AXILITE_CONFIG                       => axilite_config)
port map (
    clk => clk,
    axilite_vvc_master_if => pls_intf );

 
i_pulser : entity xil_defaultlib.ATLASPix_pulser_v1_0 
	generic map (
    -- Parameters of Axi Slave Bus Interface S00_AXI
    C_S00_AXI_DATA_WIDTH    => 32,
    C_S00_AXI_ADDR_WIDTH    =>  6)
    port map(
    -- Ports of Axi Slave Bus Interface S00_AXI
    s00_axi_aclk    => clk,
    s00_axi_aresetn  => global_reset,  
    s00_axi_awaddr   => pls_intf.write_address_channel.awaddr,   
    s00_axi_awprot   => pls_intf.write_address_channel.awprot,
    s00_axi_awvalid  => pls_intf.write_address_channel.awvalid,  
    s00_axi_awready  => pls_intf.write_address_channel.awready,   
    s00_axi_wdata    => pls_intf.write_data_channel.wdata, 
    s00_axi_wstrb    => pls_intf.write_data_channel.wstrb,
    s00_axi_wvalid   => pls_intf.write_data_channel.wvalid, 
    s00_axi_wready   => pls_intf.write_data_channel.wready, 
    s00_axi_bresp    => pls_intf.write_response_channel.bresp,  
    s00_axi_bvalid   => pls_intf.write_response_channel.bvalid, 
    s00_axi_bready   => pls_intf.write_response_channel.bready, 
    s00_axi_araddr  => pls_intf.read_address_channel.araddr,  
    s00_axi_arprot  => pls_intf.read_address_channel.arprot,   
    s00_axi_arvalid => pls_intf.read_address_channel.arvalid,    
    s00_axi_arready => pls_intf.read_address_channel.arready,    
    s00_axi_rdata   => pls_intf.read_data_channel.rdata, 
    s00_axi_rresp   => pls_intf.read_data_channel.rresp,  
    s00_axi_rvalid  => pls_intf.read_data_channel.rvalid,   
    s00_axi_rready  => pls_intf.read_data_channel.rready,    
    trg_injection   => trigger_inj,
    pulser_clk      => clk,
    EXT_PULSE_OUT_1_P  =>  ccpd_inj_pulse_1_p,  
    EXT_PULSE_OUT_1_N => ccpd_inj_pulse_1_n,
    EXT_PULSE_OUT_2_P => ccpd_inj_pulse_2_p,
    EXT_PULSE_OUT_2_N => ccpd_inj_pulse_2_n,
    EXT_PULSE_OUT_3_P => ccpd_inj_pulse_3_p,
    EXT_PULSE_OUT_3_N => ccpd_inj_pulse_3_n,
    EXT_PULSE_OUT_4_P => ccpd_inj_pulse_4_p,
    EXT_PULSE_OUT_4_N => ccpd_inj_pulse_4_n,
    EXT_PULSE_OUT_REF => ccpd_inj_ref   
    ); 




    
stim_proc : process
    variable v_cmd_idx : natural;
    begin
       await_uvvm_initialization(VOID);
       start_clock(CLOCK_GENERATOR_VVCT,1,"start clock generation");
       global_reset <= '1';

       wait for 100 ns;

       --reset
       gen_pulse(global_reset,'0',50 ns,"Reset DUT");
       wait for 100 ns;
       axilite_write(AXILITE_VVCT,2,X"0014",X"0001","writing 0000 data to reg5, reset");              
       wait for 100 ns;
       axilite_write(AXILITE_VVCT,2,X"0014",X"0000","writing 0000 data to reg5, reset");       
       
      -- config pulser
       axilite_write(AXILITE_VVCT,2,X"0010",X"000F","writing data to reg4, output_en");
       axilite_write(AXILITE_VVCT,2,X"001C",X"0001","setting default pulser level");
       wait for 500 ns;
       axilite_write(AXILITE_VVCT,2,X"0000",X"0000","writing data 0 to reg0, ccpd_inj_flg");
       axilite_write(AXILITE_VVCT,2,X"0004",X"002F","writing data to reg1, ccpd_inj_cnt");
       axilite_write(AXILITE_VVCT,2,X"0008",X"00FF","writing data to reg2, ccpd_inj_CNT_High");
       axilite_write(AXILITE_VVCT,2,X"000C",X"000F","writing data to reg3, ccpd_inj_CNT_Low");   
     
--       --fire pulser       
       axilite_write(AXILITE_VVCT,2,X"0000",X"0001","writing data 1 to reg0, ccpd_inj_flg");
       v_cmd_idx := get_last_received_cmd_idx(AXILITE_VVCT, 1);       
       await_completion(AXILITE_VVCT,1, v_cmd_idx, 100 ns, "Wait for axi transactions to finish");
        
       wait for 100 us;
       stop_clock(CLOCK_GENERATOR_VVCT,1,"stop clock generation");
       
       wait;
       
    end process;
    


end Behavioral;
